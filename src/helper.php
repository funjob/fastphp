<?php

use funjob\fast\Work;

if (!function_exists('funjob')) {
    /**
     * 入口
     */
    function funjob()
    {
        return Work::run();
    }
}
